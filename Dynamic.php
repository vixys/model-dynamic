<?php

namespace Model;

/**
 * Dynamic
 *
 * Concept of dynamic table known also as "table of tables"
 * Where the a table is a row and their fields, and bellow the records, are related to id by dynamic_id and/or dynamic_fk
 *
 * @package     Dynamic
 * @category	
 * @author	Bruno Foggia
 * @link	
 */
class Dynamic {

    use \doctrine\Dashes\Model;
//    {
//        \doctrine\Dashes\Model::findHasMany as protected _findHasMany;
//        \doctrine\Dashes\Model::getHasMany as protected _getHasMany;
//    }

    protected $table = 'dynamic';
    protected $foreignKeysSeparator = '.';
    protected $recursive = \doctrine\Dashes\HASMANY;
    protected $relatedRecursive = \doctrine\Dashes\HASMANY;
    public $foreignKeys = [
//        'parent' => [
//            'type' => \doctrine\Dashes\BELONGSTO,
//            'primaryKey' => 'ukey',
//            'key' => 'dynamic_ukey',
//            'model' => '\model\Dynamic'
//        ],
        'hasmany' => [
            'type' => \doctrine\Dashes\HASMANY,
            'primaryKey' => ['dynamic_ukey', 'ukey'],
            'key' => 'dynamic_ukey',
            'model' => '\model\Dynamic',
            'order' => 'order ASC'
        ],
    ];

    const dynamicFkRelationKey = 'dynfk_child';

    /**
     * Used to find data of dynamic item
     * @param array $ukey
     * @param int $limit
     * @param int $page
     * @param array $columns
     * @param string $orderby
     * @param int $recursive
     * @param int $relatedRecursive
     * @return array
     */
    public function getItem($ukey = [], $columns = null, $orderby = null, $recursive = null) {
        $conditions = ['ukey'=> $ukey[0],'dynamic_ukey'=> $ukey[1]];
        return $this->getBy($conditions, $columns, $orderby, $recursive);
    }
    
    /**
     * Used to find data of dynamic tables and get their relations (dynamic_id & dynamic_fk)
     * @param array $ukey
     * @param int $limit
     * @param int $page
     * @param array $columns
     * @param string $orderby
     * @param int $recursive
     * @param int $relatedRecursive
     * @return array
     */
    public function getList($ukey = [], $limit = null, $page = null, $columns = null, $orderby = null, $recursive = -1, $relatedRecursive = null) {
        (empty($ukey) || empty($ukey[1])) ?
                        ($conditions[] = "(dynamic_ukey IS NULL OR dynamic_ukey = '')") :
                        ($conditions['dynamic_ukey'] = $ukey[1]);
        !empty($ukey[0]) && ($conditions['ukey'] = $ukey[0]);
        empty($orderby) && $orderby = ['order ASC'];

        !empty($relatedRecursive) && $relatedRecursive !== -1 && ($this->relatedRecursive = $relatedRecursive);
        $results = $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
        !empty($relatedRecursive) && ($this->relatedRecursive = $this->getAttr('relatedRecursive', true));

        if (count($results) === 1) {
            if ($recursive < \doctrine\Dashes\HASMANY) {
                $results[0] = $this->getHasMany($results[0]);
            }

//            !empty($results[0]['hasmany']) && ($results[0]['hasmany'] = \Crush\Collection::transform($results[0]['hasmany'], 'ukey'));
//            $results[0]['dynfk'] = $this->getDynamicFk($results[0]['child'], $results[0]);
        }

        return $results;
    }
    
    public function getUnrelated($conditions = [], $limit = null, $page = null, $columns = null, $orderby = null) {
        $conditions[] = "(dynamic_ukey = '' OR dynamic_ukey IS NULL)";
        $conditions[] = "((SELECT COUNT(*) FROM dynamic child WHERE child.".$this->foreignKeys['hasmany']['key']." = CONCAT(IF(LENGTH(dynamic.".$this->foreignKeys['hasmany']['primaryKey'][0].")>0,CONCAT(dynamic.".$this->foreignKeys['hasmany']['primaryKey'][0].",'".$this->foreignKeysSeparator."'),''),dynamic.".$this->foreignKeys['hasmany']['primaryKey'][1].")) <= 0)";
        
        return $this->find($conditions, $limit, $page, $columns, $orderby, \doctrine\Dashes\NORELATED);
    }

//    public function getHasMany($data, $relatedNeededList = array()) {
//        $results = $this->getHasMany($data, $relatedNeededList);
//
//        !empty($results[0]['hasmany']) && ($results[0]['hasmany'] = \Crush\Collection::transform($results[0]['hasmany'], 'ukey'));
//        return $results;
//    }
//
//    public function findHasMany($results, $relatedNeededList = array()) {
//        $results = $this->_findHasMany($results, $relatedNeededList);
//        !empty($results) && ($results = array_map(function($item) {
//            !empty($item['hasmany']) && ($item['hasmany'] = \Crush\Collection::transform($item['hasmany'], 'ukey'));
//            return $item;
//        }, $results));
//        
//        return $results;
//    }

    /**
     * Estabilish a second relation that can be with it self or another table
     * The "dynamic_fk" field of the "parent" will describe who with the relation will be estabilished
     * * If "parent" dynamic_fk starts with a . it means that is a self-relation to a list named as the value without the .
     * The "dynamic_fk" field of the "list" (of childs) will determine a pkey list of the rows that have to be fetched
     * @param array $list
     * @param array $parent
     * @return array
     */
    public function getDynamicFk($list, $parent) {
        if (!empty($parent['dynamic_fk']) && !empty($list)) {
            $dynfk = explode('.', $parent['dynamic_fk']);
            $dynfkChildList = \Crush\Collection::transform($list, '', ['dynamic_fk'], ['flatten']);

            if (count($dynfk) === 2) {
                $dynfk = $dynfk[1];
                $dynfkList = \Crush\Collection::transform($this->find(['dynamic_ukey' => $dynfk, 'ukey' => $dynfkChildList]), 'ukey');
            } else {
                // integrations with other tables will be implemented later
            }

            $list = $this->mergeDynamicFk($list, $dynfkList);
        }

        return $list;
    }

    /**
     * Merge list of dynamic fk found into child list
     * @param array $dynamicList
     * @param array $dynamicFkList
     * @return array
     */
    public function mergeDynamicFk($dynamicList, $dynamicFkList) {
        foreach ($dynamicList as $x => $item) {
            if (array_key_exists($item['dynamic_fk'], $dynamicFkList)) {
                $item[self::dynamicFkRelationKey] = $dynamicFkList[$item['dynamic_fk']];
            }

            $dynamicList[$x] = $item;
        }

        return $dynamicList;
    }

}
