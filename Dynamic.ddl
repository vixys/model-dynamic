CREATE TABLE dynamic (
  id                int(11) NOT NULL AUTO_INCREMENT, 
  dynamic_ukey      varchar(100) comment 'Used to directly reference a row of this own table', 
  ukey              varchar(100) NOT NULL comment 'The not sequencial primary id to a row', 
  name              varchar(255), 
  `order`           decimal(5, 2) DEFAULT 0 comment 'Used to show ordered rows', 
  status            tinyint(1) DEFAULT 1 comment 'Defines state active or unactive of a row', 
  dynamic_fk        varchar(100) comment 'Used to undirectly reference a row of this own table OR to reference a row in other table', 
  value             text, 
  dynamic_implement varchar(100), 
  dynamic_bridge    varchar(100), 
  PRIMARY KEY (id)) engine=InnoDB;
